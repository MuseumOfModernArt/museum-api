## museum-api

This repository contains source code - and compiled code - to create a basic json rest api for Objects (by that I mean the art term, not the tech kind) in a TMS database. 

*Important!*  The source/compiled code part of this repo is not posted yet.

---

## High-level view

*Disclosure:  This api use Microsoft technologies - database, web server, and code base.*

If you are a C# programmer then you will want to clone this repo and use the source code, but other people should follow the instructions below.  

Basically you are running a SQL script, then configuring the compiled pages as a web app using Internet Information Services (IIS).  It is actually pretty easy.  Just follow the steps below.

1. Go to the Downloads link in the left menu
2. Download museum-api.sql (or get from repo src.  it is the same)
3. Download museum-api.zip

---

## SQL

The api has a fair amount of logic built into the database layer.  As such, you need some Views (6) and Stored Procedures (2) which you will find in museum-api.sql.  The SQL assumes you are running SQL Server 2008 or better.

1. Connect to your TMS database (or a copy)
2. Run the SQL

---

## Web app

The code originates from a template so it does have an actual default page (see this [example](https://alexa.moma.org/)), but in general you really care about the api methods.  More on that later.

1. Unzip museum-api.zip and put the unzipped content in a folder on the IIS server.  Personally, I would NOT use Inetpub or Program Files, just something like C:\museum_api is good.
2. Right click C:\museum_api and go to Properties; Security.  Give IIS_IUSRS Read, Read & Execute, List Folder Contents permissions.
3. Open IIS.  The simplest example assumes that there isn't a web site already running on the server.  If there is, create a new Web Site and assign a different port.  Otherwise, point the Default Web Site to C:\museum_api.  Use ASP.NET 4.5 (or 4.0) for the Application Pool.  
4. The last step is to change the database connection.  Open C:\museum_api\Web.config in a text editor and change this line (without brackets):

<add name="TMSConnectionString" connectionString="Data Source={TMSDATABASESERVER};Initial Catalog={TMSDATABASE};Persist Security Info=True;User ID={TMSDATABASEUSER};Password={USERPASSWORD};" providerName="System.Data.SqlClient"/>

* The database user should have the db_owner role for testing.  Or the user can have a minimal role (db_datareader), but then a DBA needs to run these extra commands:

- GRANT EXECUTE ON procGetObjectID TO '{TMSDATABASEUSER}'
- GRANT EXECUTE ON procApiObjects TO '{TMSDATABASEUSER}'

You should now be able to open http://myserver and see "museum-api: a rest api for tms databases""  




