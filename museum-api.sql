-- museum-api.sql
-- Run this against your TMS database.  Best to use a copy first.
-- Even better to set up a replicated database and use that.

-- Create Views

CREATE VIEW  [dbo].[vwPrimaryObjImages]
AS
SELECT MediaFiles.FileName, MediaMaster.MediaView, 
   MediaMaster.PublicCaption, Objects.ObjectID, MediaXrefs.Rank, 
   MediaMaster.Description, MediaTypes.MediaType, 
   MediaFormats.Format, MediaFiles.PixelH, MediaFiles.PixelW, 
   MediaFiles.FileSize, MediaXrefs.PrimaryDisplay, 
   MediaFiles.ColorDepthID, MediaRenditions.Quality, 
   MediaRenditions.Remarks, MediaFiles.FileDate, 
   MediaRenditions.RenditionDate, 
   MediaRenditions.RenditionNumber, 
   Objects.ObjectNumber, MediaRenditions.RenditionID, 
   MediaRenditions.MediaMasterID,
   MediaFiles.ArchIDNum,
   'http://yourinternalwebserver/Images/Size1/Images/' + MediaRenditions.ThumbFileName AS Thumbnail,
   'http://yourinternalwebserver/Images/Size3/Images/' + MediaFiles.FileName AS FullImage,
   MediaFiles.FileID,
   MediaFiles.GSRowVersion AS MediaSysTimeStamp
FROM MediaRenditions INNER JOIN
   MediaMaster ON 
   MediaRenditions.MediaMasterID = MediaMaster.MediaMasterID INNER
    JOIN
   MediaFiles ON 
   MediaRenditions.PrimaryFileID = MediaFiles.FileID INNER JOIN
   MediaXrefs ON 
   MediaMaster.MediaMasterID = MediaXrefs.MediaMasterID INNER
    JOIN
   Objects ON MediaXrefs.ID = Objects.ObjectID AND 
   MediaXrefs.TableID = 108 INNER JOIN
   MediaTypes ON 
   MediaRenditions.MediaTypeID = MediaTypes.MediaTypeID INNER
    JOIN
   MediaFormats ON 
   MediaFiles.FormatID = MediaFormats.FormatID 
 WHERE  MediaXrefs.PrimaryDisplay = 1

GO 
 
CREATE VIEW [dbo].[vwLocationsComponents] 
AS

WITH cte (object_id, locations_components) AS
(
SELECT OC.ObjectID, L.LocationString + ' (' +
STUFF((SELECT ', ' + OC1.ComponentNumber
FROM   dbo.ObjComponents AS OC1 
       INNER JOIN dbo.ObjLocations AS OL1 ON OC1.CurrentObjLocID = OL1.ObjLocationID
       LEFT OUTER JOIN dbo.Locations AS L1 ON OL1.LocationID = L1.LocationID                      
WHERE     (OL1.TransCodeID < 4)
AND OC.ObjectID = OC1.ObjectID
AND L.LocationString=L1.LocationString
AND (OC1.ComponentType = 0) AND (OL1.Inactive = 0) AND (OC1.Inactive = 0)
AND L1.LocationString IS NOT NULL
FOR XML PATH (''))
,1,2,'') + ')' AS LocationsComponents
FROM   dbo.ObjComponents AS OC 
       INNER JOIN dbo.ObjLocations AS OL ON OC.CurrentObjLocID = OL.ObjLocationID
       LEFT OUTER JOIN dbo.Locations AS L ON OL.LocationID = L.LocationID                      
WHERE     (OL.TransCodeID < 4)
AND (OC.ComponentType = 0) AND (OL.Inactive = 0) AND (OC.Inactive = 0)
AND L.LocationString IS NOT NULL
GROUP BY OC.ObjectID, L.LocationString
)
SELECT object_id AS ObjectID,
STUFF((SELECT ', ' + locations_components
FROM cte cte1
WHERE (cte1.object_id = cte2.object_id)
FOR XML PATH (''))
,1,2,'') AS LocationsComponents
FROM cte cte2
GROUP BY object_id

GO

CREATE VIEW [dbo].[vwComponentDimensions] AS
SELECT DF.ID, Dimensions = STUFF( (SELECT ',' + ('{"' + DE.Element + '":"' + REPLACE(DF1.DisplayDimensions,'"','') + '"}') 
  FROM dbo.DimensionsFlat DF1
  INNER JOIN DimensionElements DE ON DF1.ElementID = DE.ElementID
  WHERE DF1.TableID = 94
  AND DF1.ID = DF.ID
  FOR XML PATH('')), 1, 1, '')
FROM dbo.DimensionsFlat DF
WHERE DF.TableID = 94
GROUP BY DF.ID

GO

CREATE VIEW [dbo].[vwComponentAttributes] AS
SELECT TV.ID, Terms = STUFF((SELECT ',' + ('{"' + TV1.ThesXrefType + '": "' + ISNULL(TV1.Term,'') + '","Remarks": "' + ISNULL(TV1.Remarks,'') + '"}') 
  FROM dbo.TermView TV1
  WHERE TV1.TableID = 94
  AND TV1.Active = 1
  AND TV1.ID = TV.ID
  FOR XML PATH('')), 1, 1, '')
FROM dbo.TermView TV
WHERE TV.TableID = 94
AND TV.Active = 1
GROUP BY TV.ID

GO

CREATE VIEW [dbo].[vwComponentTextEntry] AS
SELECT TE.ID, TextEntry = STUFF((SELECT ',' + ('{"' + TT.TextType + '": "' + ISNULL(REPLACE(TE1.TextEntry,'"','\"'),'') + '","TextDate": "' + ISNULL(TE1.TextDate,'') 
   + '","TextAuthor": "' + ISNULL(C1.AlphaSort,'') + '"}') 
  FROM dbo.TextEntries TE1
  INNER JOIN TextTypes TT ON TE1.TextTypeID = TT.TextTypeID
  LEFT JOIN Constituents C1 ON C1.ConstituentID = TE1.AuthorConID
  WHERE TE1.TableID = 94
  AND TE1.ID = TE.ID
  FOR XML PATH('')), 1, 1, '')
FROM dbo.TextEntries TE
LEFT JOIN Constituents C ON C.ConstituentID = TE.AuthorConID
WHERE TE.TableID = 94
GROUP BY TE.ID 

GO

CREATE VIEW [dbo].[vwWebSvcComponents] AS
SELECT OC.[ComponentID]
      ,[ComponentNumber]
      ,[ComponentName]
      ,[ObjectID]
      ,[PhysDesc]
      ,[StorageComments]
      ,[InstallComments]
      ,[PrepComments]
      ,OCT.ObjCompType AS ComponentType
      ,[CompCount]
      ,'[' + CDA.Dimensions + ']' AS Dimensions
      ,'[' + CA.Terms + ']' AS Attributes
      ,'[' + TE.TextEntry + ']' AS TextEntries
  FROM [dbo].[ObjComponents] OC
   INNER JOIN [dbo].[ObjCompTypes] OCT ON OCT.ObjCompTypeID = OC.ComponentType
   LEFT JOIN [vwComponentDimensions] CDA ON OC.ComponentID = CDA.ID
   LEFT JOIN [vwComponentAttributes] CA ON OC.ComponentID = CA.ID  
   LEFT JOIN [vwComponentTextEntry] TE ON OC.ComponentID = TE.ID               
   WHERE (OC.Inactive = 0)

GO

   
-- Create Stored Procedures

CREATE PROCEDURE [dbo].[procGetObjectID]

	@p_objectnumber nvarchar(50)

AS
BEGIN

  SET NOCOUNT ON;

  SELECT O.ObjectID
  FROM [Objects] O
  LEFT JOIN [AltNums] AN ON O.ObjectID = AN.ID
  LEFT JOIN Associations A ON O.ObjectID = A.ID2   
  WHERE ObjectNumber = @p_objectnumber
  OR AN.AltNum = @p_objectnumber
  AND A.ID2 IS NULL
  GROUP BY O.ObjectID
END
 
GO

CREATE PROCEDURE [dbo].[procApiObjects]

  @p_objectid INTEGER

AS
BEGIN

SET NOCOUNT ON;

SELECT O.ObjectNumber
,O.ObjectID
,OT.Title
,Roles.Role
,ConXrefDetails.Prefix
,ConAltNames.DisplayName
,ConXrefDetails.Suffix
,ConAltNames.AlphaSort
,ISNULL(Constituents.[ConstituentID],0) AS ArtistID
,Constituents.DisplayDate
,O.Dated
,O.DateBegin
,DateEnd = CASE WHEN O.DateEnd = 0 THEN O.DateBegin ELSE O.DateEnd END
,O.Medium
,O.Dimensions
,D.Department
,D.DepartmentID
,C.Classification
,O.OnView
,O.Provenance
,O.Description
,O.ObjectStatusID
,O.CreditLine
,O.Portfolio
,O.Edition
-- LastModifiedDate removed for api example
,'' AS LastModifiedDate
,MOI.ArchIDNum AS ImageID
,MOI.Thumbnail AS Thumbnail
,MOI.FullImage AS FullImage
,L.LocationsComponents AS CurrentLocation
FROM Objects O
INNER JOIN  Departments D ON O.DepartmentID = D.DepartmentID 
INNER JOIN Classifications C ON C.ClassificationID = O.ClassificationID  
LEFT JOIN ObjTitles OT ON O.ObjectID = OT.ObjectID And OT.Displayed = 1 And OT.DisplayOrder = 1 And OT.IsExhTitle = 0
LEFT JOIN [vwLocationsComponents] L ON  O.ObjectID = L.ObjectID
LEFT JOIN [dbo].[vwPrimaryObjImages] MOI ON O.ObjectID = MOI.ObjectID
-- LEFT JOIN dbo.vwMaxLastUpdate LU ON LU.[ObjectID] = O.ObjectID    
LEFT JOIN (ConXrefs    
	INNER JOIN Roles ON ConXrefs.RoleID = Roles.RoleID    
	INNER JOIN ConXrefDetails ON ConXrefs.ConXrefID = ConXrefDetails.ConXrefID AND ConXrefDetails.UnMasked = 1    
	INNER JOIN ConAltNames ON ConXrefDetails.NameID = ConAltNames.AltNameID    
	INNER JOIN Constituents On ConXrefDetails.ConstituentID = Constituents.ConstituentID)                
ON O.ObjectID = ConXrefs.ID AND ConXrefs.RoleTypeID = 1 AND ConXrefs.TableID = 108 AND ConXrefs.DisplayOrder = 1 AND ConXrefs.Displayed = 1
WHERE O.ObjectID = @p_objectid;

SELECT E.ExhibitionID,ProjectNumber,ExhTitle,D.Department,E.DisplayDate,BeginISODate, EndISODate, (SELECT COUNT(EOX.ObjectID) FROM ExhObjXrefs EOX WHERE EOX.ExhibitionID = E.ExhibitionID)  AS ResultsCount
FROM Exhibitions E
  INNER JOIN Departments D ON D.DepartmentID = E.ExhDepartment
  INNER JOIN ExhObjXrefs EX ON E.ExhibitionID = EX.ExhibitionID 
WHERE EX.ObjectID = @p_objectid
GROUP BY E.ExhibitionID,ProjectNumber,ExhTitle,D.Department,E.DisplayDate,BeginISODate, EndISODate;

-- Constituents (aka Persons in api)
SELECT Roles.Role
,ConAltNames.DisplayName
,ConAltNames.AlphaSort
,ISNULL(Constituents.[ConstituentID],0) AS ArtistID
,Constituents.DisplayDate
,ISNULL(CAST(ConXRefs.DisplayOrder AS INT),0) AS DisplayOrder
FROM Objects O
LEFT JOIN (ConXrefs    
	INNER JOIN Roles ON ConXrefs.RoleID = Roles.RoleID    
	INNER JOIN ConXrefDetails ON ConXrefs.ConXrefID = ConXrefDetails.ConXrefID AND ConXrefDetails.UnMasked = 1    
	INNER JOIN ConAltNames ON ConXrefDetails.NameID = ConAltNames.AltNameID    
	INNER JOIN Constituents On ConXrefDetails.ConstituentID = Constituents.ConstituentID)                
ON O.ObjectID = ConXrefs.ID AND ConXrefs.TableID = 108 AND ConXrefs.Displayed = 1
WHERE O.ObjectID = @p_objectid;

SELECT [ComponentID]
	  ,[ComponentNumber]
	  ,[ComponentName]
	  ,[ObjectID]
	  ,[PhysDesc]
	  ,[StorageComments]
	  ,[InstallComments]
	  ,[PrepComments]
	  ,[ComponentType]
	  ,[CompCount]
	  ,[Dimensions]
	  ,[Attributes]
	  ,[TextEntries]
FROM [vwWebSvcComponents] C
WHERE C.ObjectID = @p_objectid

SELECT OT.[TitleTypeID]
	  ,[TitleType]
      ,[Title]
      ,[Active]
      ,[DisplayOrder]
      ,[Displayed]
      ,[IsExhTitle]
  FROM [ObjTitles] OT
  INNER JOIN [TitleTypes] TT ON OT.TitleTypeID = TT.TitleTypeID
WHERE OT.ObjectID = @p_objectid;

SELECT [EventType]
      ,[DateText]
FROM [TMS].[dbo].[ObjDates] OD
WHERE OD.ObjectID = @p_objectid;

END   
   
GO 
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   